# Boundary Stiffening CoSTs

#### Overview

Given a tetrahedral domain which is filled with a CoST, we must add additional edges to the faces of the larger tetrahedral domain.  On one of these faces, we have an arrangement of vertices that looks like a triangle with it's corners removed.  

Let `k` be the maximum number of vertices in a row of this arrangement.  If we have `k = 0,1 mod 4` then we can construct a 3-regular graph on this structure with no crossing edges which successfully stiffens the boundary on this face.

This code generates such an arrangement of vertices and constructs a correct 3-regular graph on them.  Examples of these graphs, which also show vertex labeling, can be found in `Figs`.

## Approach

#### Psuedocode 

Let `G` be the graph of vertices with initially no edges that we wish to construct a 3-regular graph on top of.  Then the pseudocode is as follows:

``` 
Stiffen(G)
	if(G.k < 8){
        G = handle_base_case(G)
	}
	else{
		[outer, innner] = peel(G)
		Stiffen(inner)
		G = connect(outer, inner)
	}
return G
```

#### An Example

Lets us look at the `k=12` case.  This will show how the pseudocode works on a graph, as well as show the indexing that we require the vertices to have for our implementation to work.  If the figures do not show up, then view this file in a proper markdown editor, such as [typora](https://typora.io/).

![k=12](k=12_example/k=12.png)

After performing the `peel` operation we lose two rings of vertices, which results in a graph with `k=12 - 4 = 8`.

![k=8](k=12_example/k=8.png)

We perform one more peel to get a graph with `k=4`:

![k=4](k=12_example/k=4.png)

We are now at our base case, which is `k=4`.  We stiffen it using a hardcoded method, and recurse up.  Stiffening the base case gives:

![k=4_fixed](k=12_example/k=4_fixed.png)

Recursing up once:

![k=8_fixed](k=12_example/k=8_fixed.png)

And finally doing our last recursion up gives the final result:

![k=12_fixed](k=12_example/k=12_fixed.png)





## Dependencies

#### [OpenVolumeMesh(OVM)](https://www.openvolumemesh.org/download/)

Once you have OVM downloaded, you have two options.

1. Install OVM onto your machine.  The cmake file `Source/src/CMakeLists.txt` looks for the file '`/usr/local/lib/libOpenVolumeMeshd.a`  If you are running a Linux distribution and OVM gets installed in its default location, this should find it.  Otherwise, the `CMakeLists.txt` file should be edited with the correct location.
2. Place the contents of `Source/src` into the folder `/Source/examples/simple_mesh/`  in the OVM directory.  Then edit `/Source/examples/CMakeLists.txt` in OVM's directory to look for `main.cpp` instead of `simple_mesh.cpp`.  Then follow OVM's directions for compiling the example.

#### [GraphViz](https://www.graphviz.org/) (Optional).

This is used to visualize the output graph.  If you choose not to view it, then simply comment out the last line from `Build/build_and_run.sh`.

## To Build

```
cd Build
cmake ../Source
./build_and_run.sh
```



## Usage

In the file `main.cpp` we initialize the value of `k`.  This value can be set to whatever value you desire, as long as `k=0,1 mod 4`.  Any other value of `k` will not work.

Running the code will generate the file `Build/stiffened_boundary.dot` which encodes the structure of the stiffened boundary.  It then runs GraphViz on it to create the final `Build/stiffened_boundary.ps` file.



## License

Boundary Stiffening is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. The GNU GPL license can be found at http://www.gnu.org/licenses/ or in the included file GNU_GPL_License.txt.