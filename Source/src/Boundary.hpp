/*
This file is part of Boundary Stiffening. 

slicing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

slicing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *  Boundary.hpp
 *
 *  Stores the boundary of one face of
 *  a tetrahedron filled with a CoST.
 */

#ifndef BOUNDARY_HH
#define BOUNDARY_HH

#include <vector>
#include <assert.h>
#include <iostream>
#include <fstream>  // file io
#include <utility>  // pair

#include <OpenVolumeMesh/Geometry/VectorT.hh>
#include <OpenVolumeMesh/Mesh/TetrahedralMesh.hh>

typedef OpenVolumeMesh::Geometry::Vec3f             Vec3f;
typedef OpenVolumeMesh::GeometricTetrahedralMeshV3f TetMeshV3f;
typedef OpenVolumeMesh::VertexHandle                vHandle;

class Boundary
{
public:
    Boundary(int size) : k(size) {};
    void write_graphviz();
    void gen_vertices();
    void stiffen();
private:
    const int k;
    std::vector<vHandle> vertices;
    TetMeshV3f myMesh;

    void gen_outer_ring();
    void gen_triangle(const Vec3f, const int);

    std::vector<vHandle> stiffen_rec(const std::vector<vHandle>, const int, int);
    std::pair<std::vector<vHandle>, std::vector<vHandle>> peel(const std::vector<vHandle>, const int);
    void stiffen_outer_ring(const std::vector<vHandle>,const int);

    std::vector<vHandle> merge(const std::vector<vHandle>, const std::vector<vHandle>, const int, const int);
    void merge_case_0(const std::vector<vHandle>, const int);
    void merge_case_1(const std::vector<vHandle>, const int);
    void merge_base_case_2(const std::vector<vHandle>);
    void merge_base_case_3(const std::vector<vHandle>);
    void merge_base_case_6(const std::vector<vHandle>);
    void merge_base_case_7(const std::vector<vHandle>);
    
    std::vector<vHandle> handle_base_case(const std::vector<vHandle>, const int);
    void handle_k_equal_2(const std::vector<vHandle>);
    void handle_k_equal_3(const std::vector<vHandle>);
    void handle_k_equal_4(const std::vector<vHandle>);
    void handle_k_equal_5(const std::vector<vHandle>);
    void handle_k_equal_6(const std::vector<vHandle>);
    void handle_k_equal_7(const std::vector<vHandle>);
};


#endif // BOUNDARY_HH
