
/*
This file is part of Boundary Stiffening. 

slicing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

slicing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *  Boundary.cpp:
 *
 *  Implements the class Boundary
 */

#include "Boundary.hpp"

using namespace std;
using namespace OpenVolumeMesh;


/*
 *  Writes out a graphviz file
 */
void Boundary::write_graphviz()
{
    ofstream(graphviz);
    graphviz.open("stiffened_boundary.dot");

    double scale = 50.0;

    graphviz << "graph G {\n";

    for(auto v_it : vertices)
    {
        auto pos = scale*myMesh.vertex(v_it); // scale
        graphviz << "   " << v_it << "[label=\"" << v_it << "\", width=0, height=0;\n";
        graphviz << "   " << "pos=\""<< pos[0] << ", " << pos[1] << "!\"];\n";
    }

    for(auto e_it=myMesh.edges_begin(); e_it!=myMesh.edges_end(); ++e_it)
    {
        auto edge = myMesh.edge(*e_it);
        vHandle to_vert = edge.to_vertex();
        vHandle from_vert = edge.from_vertex();

        graphviz << to_vert << "--" << from_vert;
        graphviz << "[color=\"black\", penwidth=0.25];\n";
    }

    graphviz << "}";

    graphviz.close();
}

/*
 *  Generates the "almost-hexagonal"
 *  array of vertices in a spiral fasion.
 *
 *  Something like this:
 *
 *            /  / \ \
 *           /  / / \ \
 *          /  / /   \ \
 *         /  / / V-> \ \
 *        /  / V ----> \ \
 *       /  V --------> \ \
 *      V -------------> \ \
 *start ---------------->   \
 *
 *  See the file "vertex_labelling.ps" to
 *  see how the vertices should be ordered
 */
void Boundary::gen_vertices()
{
    gen_outer_ring();
    gen_triangle({2.0, 1.0, 0.0}, k-2);
}


/*
 *  Calls recursive method to stiffen the graph
 */
void Boundary::stiffen()
{
    stiffen_rec(vertices, k, 1);
}


/*
 *  Generates the ring of outer vertices
 *  that form an "almost-hex," i.e. a
 *  triangle with its vertices cut off
 */
void Boundary::gen_outer_ring()
{
    Vec3f prev;
    Vec3f shift;

    // Bottom side
    prev  = Vec3f{-1.0, 0, 0};
    shift = Vec3f{2.0, 0, 0};
    for(int i=0; i<k-1; ++i)
    {
        vertices.push_back(myMesh.add_vertex(prev+shift));
        prev = prev + shift;
    }
    
    // Right side
    prev  = prev+shift;
    shift = Vec3f{-1.0, 1.0, 0.0};
    for(int i=0; i<k-1; ++i)
    {
        vertices.push_back(myMesh.add_vertex(prev+shift));
        prev = prev + shift;
    }
    
    // Left side
    prev = prev+shift;
    shift = Vec3f{-1.0, -1.0, 0.0};
    for(int i=0; i<k-1; ++i)
    {
        vertices.push_back(myMesh.add_vertex(prev+shift));
        prev = prev + shift;
    }
}

/*
 *  Given the lower left corner of a triangle
 *  with the given side lenght, fill it with 
 *  vertices.  This is a recursive function.
 */
void Boundary::gen_triangle(const Vec3f start, const int side_length)
{
    // Check floor conditions
    if(side_length < 1){
        return;
    }
    else if(side_length == 1)
    {
        vertices.push_back(myMesh.add_vertex(start));
    }

    // Add triangle
    Vec3f prev;
    Vec3f shift;

    // Bottom side
    shift = Vec3f{2.0, 0, 0};
    prev  = start;
    for(int i=0; i<side_length-1; ++i)
    {
        vertices.push_back(myMesh.add_vertex(prev+shift));
        prev = prev + shift;
    }
    
    // Right side
    shift = Vec3f{-1.0, 1.0, 0.0};
    for(int i=0; i<side_length-1; ++i)
    {
        vertices.push_back(myMesh.add_vertex(prev+shift));
        prev = prev + shift;
    }
    
    // Left side
    shift = Vec3f{-1.0, -1.0, 0.0};
    for(int i=0; i<side_length-1; ++i)
    {
        vertices.push_back(myMesh.add_vertex(prev+shift));
        prev = prev + shift;
    }

    // Recursive call
    Vec3f next_start = prev + Vec3f{3.0, 1.0, 0.0};
    gen_triangle(next_start, side_length-3);
}


/*
 *  Recursive method to stiffen the graph
 */
std::vector<vHandle> Boundary::stiffen_rec(const std::vector<vHandle> verts, const int local_k, int merge_case)
{
    /*
     *  Handle the base cases
     */
    if(local_k < 8){
        return handle_base_case(verts, local_k);
    }


    /*
     *  Split the graph into inner and outer components
     */
    auto [inner, outer] = peel(verts, local_k); // structured bindings require g++ 7 or later

    /*
     *  Stiffen outer
     */
    stiffen_outer_ring(outer, local_k);
    
    /*
     *  Stiffen inner recursively
     */
    merge_case = 1-merge_case; // case=0 -> outer contributes two edges, inner contributes one
                               // case=1 -> outer contributes one edge, inner contributes two 
    stiffen_rec(inner, local_k-6, merge_case);


    /*
     *  Combine inner and outer
     */
    std::vector<vHandle> result= merge(inner, outer, local_k, merge_case);
    
    return result;
}

/*
 *  Splits the graph into two components:
 *  (1) The outer two ring of vertices plus the inner triangle vertices
 *  (2) The inner almost-hex
 */
pair<std::vector<vHandle>, std::vector<vHandle>> Boundary::peel(const std::vector<vHandle> verts, const int local_k)
{
    std::vector<vHandle> inner;
    std::vector<vHandle> outer;

    int split_point = 3*(local_k-1) + 3*(local_k-3);
    int tri_point_1 = split_point + (local_k-7);
    int tri_point_2 = tri_point_1 + (local_k-6);
    int tri_point_3 = tri_point_2 + (local_k-6);

    // Outer ring
    for(int i=0; i<split_point; ++i)
    {
        outer.push_back(verts[i]);
    }
    // Corners of inner triangle
    outer.push_back(verts[tri_point_1]);
    outer.push_back(verts[tri_point_2]);
    outer.push_back(verts[tri_point_3]);
   
    // Inner almost-hex
    for(int i=split_point; i<verts.size(); ++i)
    {
        if(i==tri_point_1 || i==tri_point_2 || i==tri_point_3){
            continue;
        }
        inner.push_back(verts[i]);
    }

    return make_pair(inner, outer);
}


/*
 *  Given an outer two ring (plus the three triangle vertices
 *  which we ignore), generates a 3-regular graph on the vertices
 *
 *  Convention: The three points of the inner triangles are in 
 *  clockwise order starting at the lower left, and they are in 
 *  the last three indices of the verts vector
 */
void Boundary::stiffen_outer_ring(const std::vector<vHandle> verts, const int local_k)
{
    // Add outer ring of edges
    for(int i=0; i<3*(local_k-1)-1; ++i)
    {
        myMesh.add_edge(verts[i], verts[i+1]);
    }
    myMesh.add_edge(verts[3*(local_k-1)-1], verts[0]);

    // Inner ring parallel to outer ring of edges
    int offset = 3*(local_k-1);
    for(int i=offset; i< 3*(local_k-1) + 3*(local_k-3)-1; ++i)
    {
        if(i == offset + (local_k-3)-2){
            continue;
        }
        else if(i == offset + 2*(local_k-3)-2){
            continue;
        }
        else if(i == offset + 3*(local_k-3)-2){
            continue;
        }
        myMesh.add_edge(verts[i], verts[i+1]);
    }
    myMesh.add_edge(verts[offset], verts[offset + 3*(local_k-3)-1]);

    // Add the edges connecting the inner ring to the outer ring
    
    // Bottom row:
    myMesh.add_edge(verts[offset-1], verts[offset + 3*(local_k-3)-1]);
    myMesh.add_edge(verts[0], verts[offset + 3*(local_k-3)-1]);
    for(int i=1; i<(local_k-1)-2; ++i)
    {
        myMesh.add_edge(verts[i], verts[i+offset-1]);
    }
    myMesh.add_edge(verts[(local_k-1)-2], verts[(local_k-1)-2+offset-2]);
    
    // Right row:
    int right_offset = offset + local_k-4;
    myMesh.add_edge(verts[right_offset], verts[local_k-2]);
    myMesh.add_edge(verts[right_offset], verts[local_k-1]);
    for(int i=right_offset; i<right_offset+local_k-3; ++i)
    {
        myMesh.add_edge(verts[i], verts[i-right_offset+local_k-1]);
    }
    myMesh.add_edge(verts[right_offset+local_k-4], verts[2*(local_k-2)]);
    
    // Right row:
    int left_offset = right_offset + local_k-3;
    myMesh.add_edge(verts[left_offset], verts[2*local_k-3]);
    for(int i=left_offset; i<left_offset+local_k-3; ++i)
    {
        myMesh.add_edge(verts[i], verts[i-left_offset+2*local_k-2]);
    }
    myMesh.add_edge(verts[left_offset+local_k-4], verts[3*(local_k-2)+1]);
}


/*
 *  Given and inner almost hex and an outer ring,
 *  merge them and connect them in a way such that
 *  the entire graph is rigid.
 *
 *  Two steps:
 *      1) Merge the two vectors into one vector
 *      2) Add stiffening edges
 */
std::vector<vHandle> Boundary::merge(const std::vector<vHandle> inner, const std::vector<vHandle> outer, const int outer_k, const int merge_case)
{
    std::vector<vHandle> merged_verts;

    // Step 1:
    // a) Merge the two vectors
    for(const auto v : outer){
        merged_verts.push_back(v);
    }
    for(const auto v : inner){
        merged_verts.push_back(v);
    }
    
    // b) Move the triangle verts to the correct location
    //    Because of the way they were generated, this
    //    reduces to just sorting the array. 
    sort(merged_verts.begin(), merged_verts.end());

    // Step 2:
    switch (outer_k-6) {
        case 2 : merge_base_case_2(merged_verts);
                 break;
        case 3 : merge_base_case_3(merged_verts);
                 break;
        case 6 : merge_base_case_6(merged_verts);
                 break;
        case 7 : merge_base_case_7(merged_verts);
                 break;
        default: if     (merge_case==0) merge_case_0(merged_verts, outer_k);
                 else if(merge_case==1) merge_case_1(merged_verts, outer_k);
                 break;
    }

    return merged_verts;
}


/*
 *  Handles merging an outer ring with an inner
 *  almost-hex when the outer ring contributes
 *  two edges and the inner hex contributes one.
 *
 *  Sorry for the mess of indices.  It was really
 *  challenging to get it right...
 */
void Boundary::merge_case_0(const std::vector<vHandle> verts, const int local_k)
{
    /*
     *  Lower Left Vertex
     */
    int tri_1 = 3*(local_k-1) + 3*(local_k-3) + 3*(local_k-7) + 2;
    int inner_1 = tri_1-1;
    int outer_1 = 3*(local_k-1) + 3*(local_k-3) - 3;
    int outer_2 = outer_1 + 1;

    // Add the necessary edges
    myMesh.add_edge(verts[tri_1], verts[inner_1]);
    myMesh.add_edge(verts[tri_1], verts[outer_1]);
    myMesh.add_edge(verts[tri_1], verts[outer_2]);
    
    // Delete the unnecessary edges
    auto halfedge = myMesh.halfedge(verts[outer_1], verts[outer_2]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    /*
     *  Upper Vertex
     */
    tri_1 = 3*(local_k-1) + 3*(local_k-3) + 2*(local_k-7) + 1;
    inner_1 = tri_1-1;
    outer_1 = 3*(local_k-1) + 2*(local_k-3) - 3;
    outer_2 = outer_1 + 1;

    // Add the necessary edges
    myMesh.add_edge(verts[tri_1], verts[inner_1]);
    myMesh.add_edge(verts[tri_1], verts[outer_1]);
    myMesh.add_edge(verts[tri_1], verts[outer_2]);
    
    // Delete the unnecessary edges
    halfedge = myMesh.halfedge(verts[outer_1], verts[outer_2]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
   
    
    /*
     *  Lower Right Vertex
     */
    tri_1 = 3*(local_k-1) + 3*(local_k-3) + 1*(local_k-7) + 0;
    inner_1 = tri_1-1;
    outer_1 = 3*(local_k-1) + (local_k-3) - 3;
    outer_2 = outer_1 + 1;
    
    // Add the necessary edges
    myMesh.add_edge(verts[tri_1], verts[inner_1]);
    myMesh.add_edge(verts[tri_1], verts[outer_1]);
    myMesh.add_edge(verts[tri_1], verts[outer_2]);
    
    // Delete the unnecessary edges
    halfedge = myMesh.halfedge(verts[outer_1], verts[outer_2]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
}


/*
 *  Handles merging an outer ring with an inner
 *  almost-hex when the outer ring contributes
 *  one edge and the inner hex contributes two.
 *
 *  Sorry for the mess of indices.  It was really
 *  challenging to get it right...
 */
void Boundary::merge_case_1(const std::vector<vHandle> verts, const int local_k)
{
    /*
     *  Lower Left Vertex
     */
    int tri_1 = 3*(local_k-1) + 3*(local_k-3) + 3*(local_k-7) + 2;

    int inner_1 = tri_1 - (3*(local_k-7) + 2);
    int inner_2 = tri_1 - 1;
    int inner_3 = tri_1 - 3*(local_k-5) + 2;

    int outer_1 = 3*(local_k-1) - 2;
    int outer_2 = tri_1 - 3*(local_k-6);
    int outer_3 = tri_1 - 3*(local_k-6) - 1;
   
    // Add the necessary edges
    myMesh.add_edge(verts[tri_1], verts[inner_1]);
    myMesh.add_edge(verts[tri_1], verts[inner_2]);
    myMesh.add_edge(verts[tri_1], verts[inner_3]);
    
    myMesh.add_edge(verts[outer_1], verts[outer_2]);

    // Delete the uneccessary ones
    auto halfedge = myMesh.halfedge(verts[inner_1], verts[inner_2]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[outer_1], verts[outer_3]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));

    halfedge = myMesh.halfedge(verts[outer_1 + 1], verts[outer_3 + 1]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    /*
     *  Upper Vertex
     */
    tri_1 = 3*(local_k-1) + 3*(local_k-3) + 2*(local_k-7) + 1;

    inner_1 = tri_1 - (3*(local_k-5)) -1;
    inner_2 = tri_1 - 1;
    inner_3 = tri_1 + 1;

    outer_1 = inner_1;
    outer_2 = outer_1+1;
    outer_3 = 2*(local_k-2);

    // Add the necessary edges
    myMesh.add_edge(verts[tri_1], verts[inner_1]);
    myMesh.add_edge(verts[tri_1], verts[inner_2]);
    myMesh.add_edge(verts[tri_1], verts[inner_3]);
    
    myMesh.add_edge(verts[outer_2], verts[outer_3]);

    // Delete the uneccessary ones
    halfedge = myMesh.halfedge(verts[inner_2], verts[inner_3]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[outer_1], verts[outer_3]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));

    halfedge = myMesh.halfedge(verts[outer_1 + 1], verts[outer_3 + 1]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    /*
     *  Lower Right Vertex
     */
    tri_1 = 3*(local_k-1) + 3*(local_k-3) + 1*(local_k-7) + 0;

    inner_1 = tri_1 - (3*(local_k-4)) - 1;
    inner_2 = tri_1 - 1;
    inner_3 = tri_1 + 1;

    outer_1 = inner_1;
    outer_2 = outer_1+1;
    outer_3 = (local_k-2)-1;

    // Add the necessary edges
    myMesh.add_edge(verts[tri_1], verts[inner_1]);
    myMesh.add_edge(verts[tri_1], verts[inner_2]);
    myMesh.add_edge(verts[tri_1], verts[inner_3]);
    
    myMesh.add_edge(verts[outer_2], verts[outer_3]);

    // Delete the uneccessary ones
    halfedge = myMesh.halfedge(verts[inner_2], verts[inner_3]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[outer_1], verts[outer_3]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));

    halfedge = myMesh.halfedge(verts[outer_1 + 1], verts[outer_3 + 1]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
}


/*
 *  The next 4 helpers merge an outer hex with an
 *  inner almost-hex of base cases k=2, 3, 6, 7.
 *
 *  For k=4, 5 we can use the usual algorithm
 */

void Boundary::merge_base_case_2(const std::vector<vHandle> verts)
{
    // Add the required edges
    myMesh.add_edge(verts[34], verts[41]);
    myMesh.add_edge(verts[33], verts[41]);
    myMesh.add_edge(verts[40], verts[41]);

    myMesh.add_edge(verts[23], verts[37]);
    myMesh.add_edge(verts[24], verts[37]);
    myMesh.add_edge(verts[36], verts[37]);

    myMesh.add_edge(verts[28], verts[39]);
    myMesh.add_edge(verts[29], verts[39]);
    myMesh.add_edge(verts[38], verts[39]);

    // Delete the unnecessary edges
    auto halfedge = myMesh.halfedge(verts[33], verts[34]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[23], verts[24]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[28], verts[29]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
}

void Boundary::merge_base_case_3(const std::vector<vHandle> verts)
{
    // Add the required edges
    myMesh.add_edge(verts[39], verts[50]);
    myMesh.add_edge(verts[40], verts[50]);
    myMesh.add_edge(verts[42], verts[50]);

    myMesh.add_edge(verts[27], verts[44]);
    myMesh.add_edge(verts[28], verts[44]);
    myMesh.add_edge(verts[45], verts[44]);

    myMesh.add_edge(verts[33], verts[47]);
    myMesh.add_edge(verts[34], verts[47]);
    myMesh.add_edge(verts[48], verts[47]);

    // Delete the unnecessary edges
    auto halfedge = myMesh.halfedge(verts[39], verts[40]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[27], verts[28]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[33], verts[34]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
}


void Boundary::merge_base_case_6(const std::vector<vHandle> verts)
{
    // Add the required edges
    myMesh.add_edge(verts[57], verts[77]);
    myMesh.add_edge(verts[58], verts[77]);
    myMesh.add_edge(verts[76], verts[77]);

    myMesh.add_edge(verts[39], verts[65]);
    myMesh.add_edge(verts[40], verts[65]);
    myMesh.add_edge(verts[64], verts[65]);

    myMesh.add_edge(verts[48], verts[71]);
    myMesh.add_edge(verts[49], verts[71]);
    myMesh.add_edge(verts[70], verts[71]);

    // Delete the unnecessary edges
    auto halfedge = myMesh.halfedge(verts[57], verts[58]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[39], verts[40]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[48], verts[49]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
}


void Boundary::merge_base_case_7(const std::vector<vHandle> verts)
{
    // Add the required edges
    myMesh.add_edge(verts[63], verts[86]);
    myMesh.add_edge(verts[64], verts[86]);
    myMesh.add_edge(verts[66], verts[86]);

    myMesh.add_edge(verts[43], verts[72]);
    myMesh.add_edge(verts[44], verts[72]);
    myMesh.add_edge(verts[73], verts[72]);

    myMesh.add_edge(verts[53], verts[79]);
    myMesh.add_edge(verts[54], verts[79]);
    myMesh.add_edge(verts[80], verts[79]);

    // Delete the unnecessary edges
    auto halfedge = myMesh.halfedge(verts[63], verts[64]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[43], verts[44]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
    
    halfedge = myMesh.halfedge(verts[53], verts[54]);
    myMesh.delete_edge(myMesh.edge_handle(halfedge));
}
/*
 *  Handles the base cases for k=2, 3, 4, 5, 6, 7
 */
std::vector<vHandle> Boundary::handle_base_case(const std::vector<vHandle> verts, const int local_k)
{
    switch (local_k) {
        case 2 : handle_k_equal_2(verts);
                 break;
        case 3 : handle_k_equal_3(verts);
                 break;
        case 4 : handle_k_equal_4(verts);
                 break;
        case 5 : handle_k_equal_5(verts);
                 break;
        case 6 : handle_k_equal_6(verts);
                 break;
        case 7 : handle_k_equal_7(verts);
                 break;
        default: std::cout << "Unkown base case, local_k = " << local_k << std::endl;
                 break;
    }

    return verts;
}

/*
 *  The next six functions are hard-coded
 *  to handle individual base cases.
 *
 *  To understand the convention for the vertex
 *  indices, see the file "vertex_inlabelling.ps"
 */
void Boundary::handle_k_equal_2(const std::vector<vHandle> verts)
{
    assert(verts.size() == 3);
   
    myMesh.add_edge(verts[0], verts[1]);
    myMesh.add_edge(verts[1], verts[2]);
    myMesh.add_edge(verts[2], verts[0]);
}

void Boundary::handle_k_equal_3(const std::vector<vHandle> verts)
{
    assert(verts.size() == 7);
    
    for(int i=0; i<6; ++i)
    {
        myMesh.add_edge(verts[i], verts[(i+1)%6]);
    }
    
    myMesh.add_edge(verts[1], verts[6]);
    myMesh.add_edge(verts[3], verts[6]);
    myMesh.add_edge(verts[5], verts[6]);
}

/*
 *  TODO: k=4,5 can be implemented as an outer
 *        routine instead of an inner one
 */
void Boundary::handle_k_equal_4(const std::vector<vHandle> verts)
{
    assert(verts.size() == 12);
   
    for(int i=0; i<9; ++i)
    {
        myMesh.add_edge(verts[i], verts[(i+1)%9]);
    }
    
    myMesh.add_edge(verts[2], verts[9]);
    myMesh.add_edge(verts[3], verts[9]);
    myMesh.add_edge(verts[4], verts[9]);
    
    myMesh.add_edge(verts[5], verts[10]);
    myMesh.add_edge(verts[6], verts[10]);
    myMesh.add_edge(verts[7], verts[10]);
    
    myMesh.add_edge(verts[0], verts[11]);
    myMesh.add_edge(verts[1], verts[11]);
    myMesh.add_edge(verts[8], verts[11]);
}

void Boundary::handle_k_equal_5(const std::vector<vHandle> verts)
{
    assert(verts.size() == 18);
   
    for(int i=0; i<12; ++i)
    {
        myMesh.add_edge(verts[i], verts[(i+1)%12]);
    }
    
    myMesh.add_edge(verts[0], verts[17]);
    myMesh.add_edge(verts[11], verts[17]);
    myMesh.add_edge(verts[17], verts[12]);
    myMesh.add_edge(verts[1], verts[12]);
    myMesh.add_edge(verts[2], verts[12]);
    
    myMesh.add_edge(verts[3], verts[13]);
    myMesh.add_edge(verts[4], verts[13]);
    myMesh.add_edge(verts[5], verts[14]);
    myMesh.add_edge(verts[6], verts[14]);
    myMesh.add_edge(verts[13], verts[14]);
    
    myMesh.add_edge(verts[7], verts[15]);
    myMesh.add_edge(verts[8], verts[15]);
    myMesh.add_edge(verts[9], verts[16]);
    myMesh.add_edge(verts[10], verts[16]);
    myMesh.add_edge(verts[15], verts[16]);
}

void Boundary::handle_k_equal_6(const std::vector<vHandle> verts)
{
    assert(verts.size() == 25);
   
    for(int i=0; i<15; ++i)
    {
        myMesh.add_edge(verts[i], verts[(i+1)%15]);
    }
    
    myMesh.add_edge(verts[0], verts[23]);
    myMesh.add_edge(verts[1], verts[15]);
    myMesh.add_edge(verts[2], verts[16]);
    myMesh.add_edge(verts[23], verts[15]);
    myMesh.add_edge(verts[15], verts[16]);
    myMesh.add_edge(verts[13], verts[23]);
    
    myMesh.add_edge(verts[3], verts[17]);
    myMesh.add_edge(verts[17], verts[18]);
    myMesh.add_edge(verts[18], verts[19]);
    myMesh.add_edge(verts[5], verts[17]);
    myMesh.add_edge(verts[6], verts[18]);
    myMesh.add_edge(verts[7], verts[19]);
    
    myMesh.add_edge(verts[8], verts[20]);
    myMesh.add_edge(verts[10], verts[20]);
    myMesh.add_edge(verts[11], verts[21]);
    myMesh.add_edge(verts[12], verts[22]);
    myMesh.add_edge(verts[20], verts[21]);
    myMesh.add_edge(verts[21], verts[22]);
    
    myMesh.add_edge(verts[16], verts[24]);
    myMesh.add_edge(verts[19], verts[24]);
    myMesh.add_edge(verts[22], verts[24]);
}

void Boundary::handle_k_equal_7(const std::vector<vHandle> verts)
{
    assert(verts.size() == 33);
   
    for(int i=0; i<18; ++i)
    {
        if(i==0 || i==6 || i==12){
            continue;
        }
        myMesh.add_edge(verts[i], verts[(i+1)%18]);
    }
    
    myMesh.add_edge(verts[0], verts[29]);
    myMesh.add_edge(verts[1], verts[18]);
    myMesh.add_edge(verts[2], verts[19]);
    myMesh.add_edge(verts[3], verts[20]);
    myMesh.add_edge(verts[17], verts[29]);
    myMesh.add_edge(verts[29], verts[1]);
    myMesh.add_edge(verts[18], verts[19]);
    myMesh.add_edge(verts[19], verts[20]);
    myMesh.add_edge(verts[20], verts[4]);
    
    myMesh.add_edge(verts[5], verts[21]);
    myMesh.add_edge(verts[6], verts[21]);
    myMesh.add_edge(verts[7], verts[21]);
    myMesh.add_edge(verts[7], verts[22]);
    myMesh.add_edge(verts[8], verts[23]);
    myMesh.add_edge(verts[9], verts[24]);
    myMesh.add_edge(verts[22], verts[23]);
    myMesh.add_edge(verts[23], verts[24]);
    myMesh.add_edge(verts[24], verts[10]);
    
    myMesh.add_edge(verts[11], verts[25]);
    myMesh.add_edge(verts[12], verts[25]);
    myMesh.add_edge(verts[13], verts[25]);
    myMesh.add_edge(verts[26], verts[27]);
    myMesh.add_edge(verts[27], verts[28]);
    myMesh.add_edge(verts[13], verts[26]);
    myMesh.add_edge(verts[14], verts[27]);
    myMesh.add_edge(verts[15], verts[28]);
    myMesh.add_edge(verts[16], verts[28]);
    
    myMesh.add_edge(verts[18], verts[32]);
    myMesh.add_edge(verts[22], verts[30]);
    myMesh.add_edge(verts[26], verts[31]);
    
    myMesh.add_edge(verts[30], verts[31]);
    myMesh.add_edge(verts[31], verts[32]);
    myMesh.add_edge(verts[32], verts[30]);
}































